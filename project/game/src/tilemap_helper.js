class TilemapHelper {
	constructor(map, scaleFactor) {
		this.map = map; 
		this.scaleFactor = scaleFactor; 
	}
	loadObjects(gfxItems, groups) {
		for (var object_layer in this.map.objects) {
	    	if (this.map.objects.hasOwnProperty(object_layer)) {	
	    		for(var objectKey in this.map.objects[object_layer]) { 
	    			var object = this.map.objects[object_layer][objectKey]; 
	            	var position = {"x": Math.round((object.x + (this.map.tileHeight / 2))*this.scaleFactor), "y": Math.round((object.y - (this.map.tileHeight / 2))*this.scaleFactor)};	// object pos is tile center  
				    switch (object.type) {	// create object according to its type
			    	case "player": // the player and its settings
			    		gfxItems.playerSprite = this.map.game.add.sprite(position.x, position.y, 'player');	
				   		gfxItems.playerSprite.scale.set(scaleFactor); 
					    this.map.game.physics.arcade.enable(gfxItems.playerSprite);	//  We need to enable physics on the sprite  
					    gfxItems.playerSprite.body.width = 55;	// Decrease body width to improve collisions with platforms 
					    gfxItems.playerSprite.body.collideWorldBounds = true;
					    gfxItems.playerSprite.animations.add('walk', [1, 2, 3, 4, 5, 6], 6, true);	//  Our animation, walking left and right.
					    gfxItems.playerSprite.frame = 3;
					    gfxItems.playerSprite.anchor.setTo(0.5);
			    		break;
				    case "ground_enemy":	// the enemy object and its settings 	
				    	var enemy = { sprite: null, dir: 0, maxDist: 0, speed: 0, initialX: 0 };
				    	enemy.sprite = game.add.sprite(position.x, position.y, 'slime');
				    	enemy.sprite.scale.set(scaleFactor, scaleFactor); 
				    	enemy.sprite.anchor.setTo(.5,.5);	// texture center is origin 
				    	this.map.game.physics.arcade.enable(enemy.sprite);	//  We need to enable physics on the sprite  
				    	enemy.dir = object.properties.direction; 
				    	enemy.maxDist = object.properties.walking_distance; 
				    	enemy.speed = object.properties.walking_speed; 
				    	enemy.initialX = enemy.sprite.body.x; 
					    groups['enemies'].add(enemy.sprite);  
					    gfxItems.enemies[object.name] = enemy; 
					    break;
				    case "goal":	// the goal object and its settings 	
				    	gfxItems.goalSprite = game.add.sprite(position.x, position.y, 'goal');
				    	gfxItems.goalSprite.scale.set(scaleFactor, scaleFactor); 
				    	this.map.game.physics.arcade.enable(gfxItems.goalSprite);	//  We need to enable physics on the sprite  
				    	break;
				    }
				}
	    	}
	    }
	}
}